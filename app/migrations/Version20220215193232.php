<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220215193232 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE find_dictionary DROP FOREIGN KEY FK_5DB1C7B4166D1F9C');
        $this->addSql('ALTER TABLE find_dictionary DROP FOREIGN KEY FK_5DB1C7B4D96C566B');
        $this->addSql('ALTER TABLE find_dictionary ADD CONSTRAINT FK_5DB1C7B4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE find_dictionary ADD CONSTRAINT FK_5DB1C7B4D96C566B FOREIGN KEY (path_id) REFERENCES path_dictionary (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE find_dictionary DROP FOREIGN KEY FK_5DB1C7B4D96C566B');
        $this->addSql('ALTER TABLE find_dictionary DROP FOREIGN KEY FK_5DB1C7B4166D1F9C');
        $this->addSql('ALTER TABLE find_dictionary ADD CONSTRAINT FK_5DB1C7B4D96C566B FOREIGN KEY (path_id) REFERENCES path_dictionary (id)');
        $this->addSql('ALTER TABLE find_dictionary ADD CONSTRAINT FK_5DB1C7B4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }
}
