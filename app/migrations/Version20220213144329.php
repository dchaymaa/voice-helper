<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220213144329 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE find_dictionary (id INT AUTO_INCREMENT NOT NULL, path_id INT NOT NULL, project_id INT NOT NULL, words LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_5DB1C7B4D96C566B (path_id), INDEX IDX_5DB1C7B4166D1F9C (project_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE find_dictionary ADD CONSTRAINT FK_5DB1C7B4D96C566B FOREIGN KEY (path_id) REFERENCES path_dictionary (id)');
        $this->addSql('ALTER TABLE find_dictionary ADD CONSTRAINT FK_5DB1C7B4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE find_dictionary');
    }
}
