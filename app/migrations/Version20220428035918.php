<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220428035918 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE path_dictionary ADD payload VARCHAR(255) NOT NULL, CHANGE path type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE unprocessed_path DROP FOREIGN KEY FK_43798556166D1F9C');
        $this->addSql('ALTER TABLE unprocessed_path ADD CONSTRAINT FK_43798556166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE path_dictionary ADD path VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, DROP type, DROP payload');
        $this->addSql('ALTER TABLE unprocessed_path DROP FOREIGN KEY FK_43798556166D1F9C');
        $this->addSql('ALTER TABLE unprocessed_path ADD CONSTRAINT FK_43798556166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }
}
