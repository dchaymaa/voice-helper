<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PathDictionaryRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PathDictionaryRepository::class)
 */
class PathDictionary
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"main"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"main"})
     */
    private $payload;

    /**
     * @ORM\Column(type="array")
     * @Groups({"main"})
     */
    private $parameters = [];

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"main"})
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"main"})
     */
    private $needAuth;

    // Payload types
    const TYPE_MESSAGE = 'message';
    const TYPE_ROUTE = 'route';
    const TYPE_FUNCTION = 'function';
    const TYPE_DIALOG = 'dialog';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, [
            self::TYPE_MESSAGE,
            self::TYPE_ROUTE,
            self::TYPE_FUNCTION,
            self::TYPE_DIALOG,

        ])) {
            throw new \InvalidArgumentException("Invalid payload type ${type}");
        }

        $this->type = $type;

        return $this;
    }

    public function getPayload(): ?string
    {
        return $this->payload;
    }

    public function setPayload(string $payload): self
    {
        $this->payload = $payload;

        return $this;
    }

    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters): self
    {
        foreach ($parameters as $key => $value) {
            if ($value === "true") $value = true;
            elseif ($value === "false") $value = false;

            if (is_string($value) && intval($value) && strlen(strval(intval($value))) === strlen($value)) $value = intval($value);

            if (is_string($value) && floatval($value) && strlen(strval(floatval($value))) === strlen($value)) $value = floatval($value);

            $parameters[$key] = $value;
        }
        $this->parameters = $parameters;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNeedAuth(): ?bool
    {
        return $this->needAuth;
    }

    public function setNeedAuth(?bool $needAuth): self
    {
        $this->needAuth = $needAuth;

        return $this;
    }
}
