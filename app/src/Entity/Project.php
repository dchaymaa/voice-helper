<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProjectRepository;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 */
class Project
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"main"})
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"main"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $manager;

    /**
     * @ORM\OneToMany(targetEntity=UnprocessedPath::class, mappedBy="project")
     */
    private $unprocessedPaths;

    public function __construct()
    {
        $this->unprocessedPaths = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return Collection<int, UnprocessedPath>
     */
    public function getUnprocessedPaths(): Collection
    {
        return $this->unprocessedPaths;
    }

    public function addUnprocessedPath(UnprocessedPath $unprocessedPath): self
    {
        if (!$this->unprocessedPaths->contains($unprocessedPath)) {
            $this->unprocessedPaths[] = $unprocessedPath;
            $unprocessedPath->setProject($this);
        }

        return $this;
    }

    public function removeUnprocessedPath(UnprocessedPath $unprocessedPath): self
    {
        if ($this->unprocessedPaths->removeElement($unprocessedPath)) {
            // set the owning side to null (unless already changed)
            if ($unprocessedPath->getProject() === $this) {
                $unprocessedPath->setProject(null);
            }
        }

        return $this;
    }
}
