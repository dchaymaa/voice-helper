<?php

namespace App\Entity;

use App\Repository\FindDictionaryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=FindDictionaryRepository::class)
 */
class FindDictionary
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\Column(type="array")
     * @Groups({"main"})
     */
    private $words = [];

    /**
     * @ORM\ManyToOne(targetEntity=PathDictionary::class)
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @Groups({"main"})
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class)
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @Groups({"main"})
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWords(): ?array
    {
        return $this->words;
    }

    public function setWords(array $words): self
    {
        $this->words = $words;

        return $this;
    }

    public function getPath(): ?PathDictionary
    {
        return $this->path;
    }

    public function setPath(?PathDictionary $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}
