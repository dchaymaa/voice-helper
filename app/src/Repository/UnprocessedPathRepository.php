<?php

namespace App\Repository;

use App\Entity\UnprocessedPath;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UnprocessedPath|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnprocessedPath|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnprocessedPath[]    findAll()
 * @method UnprocessedPath[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnprocessedPathRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UnprocessedPath::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(UnprocessedPath $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(UnprocessedPath $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return UnprocessedPath[] Returns an array of UnprocessedPath objects
    //  */

    public function incrementCount($entity)
    {
        return $this->createQueryBuilder('u')
            ->update($this->getEntityName(), 'u')
            ->set('u.count', 'u.count + 1')
            ->where('u.id = :id')
            ->setParameter('id', $entity->getId())
            ->getQuery()
            ->execute()
        ;
    }

    /*
    public function findOneBySomeField($value): ?UnprocessedPath
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
