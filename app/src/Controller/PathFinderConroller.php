<?php

namespace App\Controller;

use App\Entity\FindDictionary;
use App\Entity\PathDictionary;
use App\Entity\Project;
use App\Entity\UnprocessedPath;
use App\Service\NormalizeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/path-finder")
 */
class PathFinderConroller extends AbstractController
{
    /**
     * @Route("/", name="get_path", methods={"POST"})
     */
    public function findPath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(is_null($project = $em->getRepository(Project::class)->findOneByCode($data['project'] ?? '')))
            return $this->json([
                'message' =>'Не существует такого проекта'
        ], Response::HTTP_NOT_FOUND);

        $paths = $em->getRepository(FindDictionary::class)->findByProject($project);
        $text = $data['text'] ?? '';

        $max = 0;
        $path = null;
        foreach($paths as $finderPath) {
            $count = 0;
            $words = $finderPath->getWords();
            foreach($words as $word) {
                if(strpos(mb_strtolower($text, 'UTF-8'), $word) !== false) $count++;
            }
            if($count >= $max && $count == count($words)) {
                $path = $finderPath->getPath();
                $max = $count;
            }
        }

        if (!empty($path))
            return $this->json([
                'message' => 'Найденный путь',
                'data' => (new NormalizeService())->normalizeByGroup($path),
            ]);

        $text = mb_strtolower($text);
        $repository = $em->getRepository(UnprocessedPath::class);
        $up = $repository->findOneByText($text);

        if (!empty($up)) {
            $repository->incrementCount($up);
        }
        else {
            $new_up = new UnprocessedPath();
            $new_up
                ->setProject($project)
                ->setText($text);

            $em->persist($new_up);
            $em->flush();
        }

        return $this->json([
            'message' => 'Путь не найден!',
        ]);

    }

    /**
     * @Route("/project", name="get_paths_for_project", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getProjectsPaths(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if(
            is_null($project = $em->getRepository(Project::class)
                ->findOneByCode($request->query->get('project', '')))
        )
            return $this->json([
                'message' =>'Не существует такого проекта'
            ], Response::HTTP_NOT_FOUND);

        if(
            !in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($project->getManager())
            || $project->getManager()->getId() != $this->getUser()->getId())
        )
            return $this->json([
                'message' =>'Вы не назначены на данный проект'
            ], Response::HTTP_FORBIDDEN);

        $path = $em->getRepository(FindDictionary::class)->findByProject($project);

        return $this->json([
            'message' => 'Пути и словарь слов для данного проекта',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/all", name="get_all_from_dictionary", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function getAllPaths()
    {
        $em = $this->getDoctrine()->getManager();
        $path = $em->getRepository(FindDictionary::class)->findAll();

        return $this->json([
            'message' => 'Все пути и словари слов',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/create", name="add_to_find_dictionary", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function addPathToDictionary(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(
            is_null($path = $em->getRepository(PathDictionary::class)->find($data['path'] ?? ''))
            || empty($words = $data['words'] ?? null)
            || empty($code = $data['project'] ?? null)
        )
            return $this->json([
                'message' =>'Не введена вся требуемая информация'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        if(is_null($project = $em->getRepository(Project::class)->findOneByCode($code)))
            return $this->json([
                'message' =>'Не существует такого проекта'
            ], Response::HTTP_NOT_FOUND);

        if(
            !in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($project->getManager())
            || $project->getManager()->getId() != $this->getUser()->getId())
        )
            return $this->json([
                'message' =>'Вы не назначены на данный проект'
            ], Response::HTTP_FORBIDDEN);

        if(is_array($words)) {
            $lc_words = array();
            foreach($words as $word) {
                $word = mb_strtolower($word, 'UTF-8');
                $lc_words[] = $word;
            }
            $words = $lc_words;
        }
        else $words = mb_strtolower($words, 'UTF-8');

        $info = new FindDictionary();
        $info->setPath($path);
        $info->setProject($project);
        $info->setWords(
            is_array($words) ? $words : array($words)
        );

        $em->persist($info);
        $em->flush();

        return $this->json([
            'message' => 'Информация добавлена в словарь поиска',
            'data' => (new NormalizeService())->normalizeByGroup($info),
        ]);
    }

    /**
     * @Route("/update", name="edit_dictionary_info", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function editDictionaryInfo(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(
            is_null($info = $em->getRepository(FindDictionary::class)
                ->find($data['info'] ?? ''))
        )
            return $this->json([
                'message' => 'В словаре нет такого элемента'
            ], Response::HTTP_NOT_FOUND);

        if(
            !in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($info->getProject()->getManager())
            || $info->getProject()->getManager()->getId() != $this->getUser()->getId())
        )
            return $this->json([
                'message' =>'Вы не назначены на данный проект'
            ], Response::HTTP_FORBIDDEN);

        $info->setProject(
            is_null($project = $em->getRepository(Project::class)->findOneByCode($data['project'] ?? '')) ?
            $info->getProject() : $project
        );
        $info->setPath(
            is_null($path = $em->getRepository(PathDictionary::class)->find($data['path'] ?? '')) ?
            $info->getPath() : $path
        );

        $words = $data['words'] ?? $info->getWords();

        if(is_array($words)) {
            $lc_words = array();
            foreach($words as $word) {
                $word = mb_strtolower($word, 'UTF-8');
                $lc_words[] = $word;
            }
            $words = $lc_words;
        }
        else $words = mb_strtolower($words, 'UTF-8');

        $info->setWords(
            is_array($words) ? $words : array($words)
        );

        $em->flush();

        return $this->json([
            'message' => 'Информация в словаре успешно обновлена',
            'data' => (new NormalizeService())->normalizeByGroup($info),
        ]);
    }

    /**
     * @Route("/words", name="edit_words", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function editWords(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(
            is_null($info = $em->getRepository(FindDictionary::class)
                ->find($data['info'] ?? ''))
        )
            return $this->json([
                'message' =>'В словаре нет такого элемента'
            ], Response::HTTP_NOT_FOUND);

        if(
            !in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($info->getProject()->getManager())
            || $info->getProject()->getManager()->getId() != $this->getUser()->getId())
        )
            return $this->json([
                'message' =>'Вы не назначены на данный проект'
            ], Response::HTTP_FORBIDDEN);

        if(!is_null($words = $data['words'] ?? null)) {
            $type = $data['type'] ?? 'add';
            if(is_array($words)) {
                $lc_words = array();
                foreach($words as $word) {
                    $word = mb_strtolower($word, 'UTF-8');
                    $lc_words[] = $word;
                }
                $words = $lc_words;
            }
            else $words = mb_strtolower($words, 'UTF-8');

            if($type == 'add') {
                $array = $info->getWords();
                $array = array_merge($array, is_array($words) ? $words : [$words]);
                $info->setWords(array_unique($array, SORT_REGULAR));
            }
            elseif($type == 'remove') {
                $info->setWords(array_values(
                    array_diff($info->getWords(), is_array($words) ? $words : [$words]))
                );
            }
        }

        $em->flush();

        return $this->json([
            'message' => 'Изменен набор слов для данного элемента словаря',
            'data' => (new NormalizeService())->normalizeByGroup($info),
        ]);
    }

    /**
     * @Route("/", name="remove_from_dictionary", methods={"DELETE"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function removeFromFindDictionary(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(
            is_null($info = $em->getRepository(FindDictionary::class)
                ->find($data['info'] ?? ''))
        )
            return $this->json([
                'message' =>'В словаре нет такого элемента'
            ], Response::HTTP_NOT_FOUND);

        if(
            !in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($info->getProject()->getManager())
            || $info->getProject()->getManager()->getId() != $this->getUser()->getId())
        )
            return $this->json([
                'message' =>'Вы не назначены на данный проект'
            ], Response::HTTP_FORBIDDEN);

        $em->remove($info);
        $em->flush();

        return $this->json([
            'message' => 'Требуемая информаця успешно удалена из словаря',
        ]);
    }
}
